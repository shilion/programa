<?php
require_once "bootstrap.php";
$productRepository = $entityManager->getRepository(\Zadanie6\Entity\Product::class);
$categoryRepository = $entityManager->getRepository(\Zadanie6\Entity\Category::class);
$availableProducts = $productRepository->countAvailableProducts();
$unavailableProducts = $productRepository->findUnavailableProducts();
$productsByName = $productRepository->findByName('produkt');
$availableProductsInCategory = $categoryRepository->countAvailableProductsInCategory(1);
$productsSortedAlphabetical = $categoryRepository->findProductsByCategoryAndSort(2);

echo "<pre>";
echo "<b>Liczba dostępnych produktów: </b>" . $availableProducts;
echo "</pre>";

echo "<pre>";
echo "<br><b>Kolekcja niedostępnych produktów: </b><br>";

/** @var \Zadanie6\Entity\Product $unavailableProduct */
foreach ($unavailableProducts as $unavailableProduct) {
    echo "<br>";
    echo "Nazwa: " . $unavailableProduct->getName() . "<br>";
    echo "Opis: " . $unavailableProduct->getDescription() . "<br>";
    echo "Cena: " . $unavailableProduct->getPrice() . "<br>";
}
echo "</pre>";

echo "<pre>";
echo "<br><b>Kolekcja produktów mających w nazwie 'produkt': </b><br>";

/** @var \Zadanie6\Entity\Product $product */
foreach ($productsByName as $product) {
    $available = $product->isAvailable() ? 'Tak' : 'Nie';
    echo "<br>";
    echo "Nazwa: " . $product->getName() . "<br>";
    echo "Opis: " . $product->getDescription() . "<br>";
    echo "Cena: " . $product->getPrice() . "<br>";
    echo "Dostępny: " . $available  . "<br>";
}
echo "</pre>";

echo "<pre>";
echo "<b>Liczba dostępnych produktów w kategorii o id = 1: </b>" . $availableProductsInCategory;
echo "</pre>";

echo "<pre>";
echo "<br><b>Kolekcja produktów w kategorii o id = 2, posortowana alfebetycznie: </b><br>";
/** @var \Zadanie6\Entity\Product $product */
foreach ($productsSortedAlphabetical as $product) {
    $available = $product->isAvailable() ? 'Tak' : 'Nie';
    echo "<br>";
    echo "Nazwa: " . $product->getName() . "<br>";
    echo "Opis: " . $product->getDescription() . "<br>";
    echo "Cena: " . $product->getPrice() . "<br>";
    echo "Dostępny: " . $available  . "<br>";
}
echo "</pre>";
