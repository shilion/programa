<?php

namespace Zadanie6\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package Zadanie6\Entity
 *
 * @ORM\Entity(repositoryClass="Zadanie6\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="categories", fetch="EXTRA_LAZY")
     */
    private $products;

    public function __construct()
    {
        parent::__construct();

        $this->products = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        $this->products->contains($product) ? null : $this->products->add($product);

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->contains($product) ? $this->products->removeElement($product) : null;

        return $this;
    }
}
