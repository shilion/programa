<?php

namespace Zadanie6\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product
 * @package Zadanie6\Entity
 *
 * @ORM\Entity(repositoryClass="Zadanie6\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $available;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="products", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="product_category")
     */
    private $categories;

    public function __construct()
    {
        parent::__construct();

        $this->categories = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Product
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function isAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(?bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getCategories(): ?Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        $this->categories->contains($category) ? null : $this->categories->add($category);

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->contains($category) ? $this->categories->removeElement($category) : null;

        return $this;
    }
}
