<?php

use Zadanie6\Entity\Product;
use Doctrine\ORM\EntityManager;
use Zadanie6\Entity\Category;

require_once(__DIR__. '/../../bootstrap.php');

if (!empty($entityManager)) {
    load($entityManager);
}

function load(
    EntityManager $entityManager
): void {
    $categoriesNames = ['AGD', 'RTV', 'Telefony', 'Komputery', 'Inne', 'Lodówki'];
    $productsNames = ['Pralka', 'Lodówka', 'Smartphone', 'Losowy produkt', 'Inny produkt'];
    $categoriesGroupOne = [];
    $categoriesGroupTwo = [];
    $products = [];

    for ($i=1; $i<= 3; $i++) {
        $categoriesGroupOne[$i] = createCategory($categoriesNames[$i-1]);
    }

    for ($i=4; $i<= 6; $i++) {
        $categoriesGroupTwo[$i - 3] = createCategory($categoriesNames[$i-1]);
    }

    foreach ($categoriesGroupOne as $category) {
        $entityManager->persist($category);
    }

    foreach ($categoriesGroupTwo as $category) {
        $entityManager->persist($category);
    }

    $entityManager->flush();

    for ($i=1; $i <= 10; $i++) {
        $products[] = createProduct(
            $productsNames[$i % 5],
            'Opis '. $i,
            199.90,
            true,
            $categoriesGroupOne
        );
    }

    for ($i=11; $i <= 20; $i++) {
        $products[] = createProduct(
            $productsNames[$i % 5],
            'Opis '. $i,
            20.90,
            false,
            $categoriesGroupTwo
        );
    }

    foreach ($products as $product) {
        $entityManager->persist($product);
    }

    $entityManager->flush();

}

function createProduct(
    string $name,
    ?string $description,
    float $price,
    ?bool $available,
    ?array $categories
): Product {
    $product = new Product();

    foreach ($categories as $category) {
        $product->addCategory($category);
    }

    return $product
        ->setName($name)
        ->setDescription($description)
        ->setPrice($price)
        ->setAvailable($available);
}

function createCategory(
    string $name
): Category {
    $category = new Category();

    return $category->setName($name);
}
