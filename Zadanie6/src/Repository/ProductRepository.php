<?php

namespace Zadanie6\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function countAvailableProducts()
    {
        $builder = $this->createQueryBuilder('en');

        $builder
            ->select('count(en.id)')
            ->andWhere('en.available = :true')
            ->setParameter('true', true)
        ;

        return $builder->getQuery()->getSingleScalarResult();
    }

    public function findUnavailableProducts()
    {
        $builder = $this->createQueryBuilder('en');

        $builder
            ->andWhere('en.available = :false')
            ->setParameter('false', false)
        ;

        return $builder->getQuery()->getResult();
    }

    public function findByName(
        string $name
    ) {
        $builder = $this->createQueryBuilder('en');

        $builder
            ->andWhere('en.name LIKE :name')
            ->setParameter('name', '%' . $name . '%')
        ;

        return $builder->getQuery()->getResult();
    }
}
