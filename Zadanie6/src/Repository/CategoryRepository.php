<?php

namespace Zadanie6\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function countAvailableProductsInCategory(int $categoryId)
    {
        $builder = $this->createQueryBuilder('en');

        $builder->innerJoin('en.products', 'pr');

        $builder
            ->select('count(en.id)')
            ->andWhere('en.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->andWhere('pr.available = :true')
            ->setParameter('true', true)
        ;

        return $builder->getQuery()->getSingleScalarResult();
    }

    public function findProductsByCategoryAndSort(
        int $categoryId
    ) {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('pr')
            ->from('Zadanie6\Entity\Product', 'pr')
            ->innerJoin('pr.categories', 'en')
            ->andWhere('en.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->orderBy('pr.name', 'ASC')
        ;

        return $builder->getQuery()->getResult();
    }
}
