# Zadanie 6

## Treść zadania
Korzystając z Doctrine utwórz model opisujący produkt w sklepie internetowym. Produkt powinien być
opisany przez następujące pola:
* nazwa - wymagana,
* opis,
* cena - wymagana,
* dostępność - bool.

Utwórz klasę repozytorium dla tego modelu, która będzie posiadała następujące metody:
* zwracającą liczbę dostępnych produktów,
* zwracającą kolekcję niedostępnych produktów,
* zwracającą kolekcję produktów, które w nazwie posiadają zadaną frazę.

Dodaj model kategorii i powiązanie z produktem. Jeden produkt może należeć do wielu kategorii. Utwórz
repozytorium dla kategorii, które pozwoli w łatwy sposób:
* pobrać liczbę produktów dostępnych w konkretnej kategorii,
* pobrać listę produktów z danej kategorii posortowanych alfabetycznie.

## Wykorzystane biblioteki
 - `doctrine/orm`
 
## Start aplikacji
 ```
 1) Tworzymy bazę danych
 2) Edytujemy plik 'bootstrap.php' i uzupełniamy go o dane utworzonej bazy
 3) Wykonujemy composer install
 4) Wykonujemy skrypt './load_fixtures.sh' (tworzy on tabele w bazie danych i wypełnia je danymi)
 5) Wchodzimy na stronę główną ("index.php")
 ```
 
## Przydatne komendy
 
 ```
- vendor/bin/doctrine orm:schema-tool:drop --force - usunięcie tabel w bazie danych
- vendor/bin/doctrine orm:schema-tool:update --force - aktualizacja tabel w bazie danych
- php src/DataFixtures/ProductData.php - załadowanie wcześniej spreparowanych danych do tabel
```