# Zadanie 2, 3, 4, 5

## Wykorzystane biblioteki
 - `php-di/php-di`
 
## Start aplikacji
 ```
 1) Tworzymy bazę danych
 2) Edytujemy plik 'SqlConfigTrait.php' znajdujący się w katalogu 'src/Traits/Sql' i uzupełniamy go o dane utworzonej bazy
 3) Wykonujemy composer install
 4) Wykonujemy w terminalu komendę 'php console.php' (tworzy ona tabele w bazie danych i wypełnia je danymi)
 5) Wchodzimy na stronę główną ("index.php")
 ```