<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Zadanie 2, 3, 4, 5</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <script type="text/javascript" src='./public/js/validate.js'></script>
</head>

<body>
<div id="menu">
    <ul>
        <?= $menu ?>
    </ul>
</div>

<article>
    <form action="", method="post" onsubmit="return validate()">
        First name:<br>
        <input type="text" name="firstname" id="firstname"><br>
        <p id="firstname-error" style="color: red"></p>
        Last name:<br>
        <input type="text" name="lastname" id="lastname"><br>
        <p id="lastname-error" style="color: red"></p>
        Age:<br>
        <input type="text" name="age" id="age"><br>
        <p id="age-error" style="color: red"></p>
        Gender:<br>
        <input type="radio" name="gender" value="male"> Male<br>
        <input type="radio" name="gender" value="female"> Female<br>
        <input type="radio" name="gender" value="other"> Other<br>
        <p id="gender-error" style="color: red"></p>
        Processing Of Personal Data:<br>
        <input type="checkbox" name="data-proccessing" id="data-proccessing">Agreed<br>
        <p id="data-proccessing-error" style="color: red"></p>
        <input type="submit" value="Submit">
    </form>
</article>

<article>
    <header>
        <h1>Curriculum Vitae</h1>
    </header>

    <section id="personal-data">
        <h1>Hubert Sitarski</h1>
        <p>Email: hubertsitarski@gmail.com</p>
        <p>Telefon: 514 331 470</p>
        <p>Data urodzenia: 21.06.1997</p>
        <p>Miejscowość: Poznań</p>
    </section>

    <section id="experience">
        <h1>Doświadczenie zawodowe</h1>
        <p>02.2018 - obecnie: PHP Developer - Globe Group Sp. z.o.o.</p>
        <p>05.2017 - 02.2018: Programista PHP - Web Developer  - Bloomboard Group Sp. z.o.o.</p>
    </section>

    <section id="education">
        <h1>Wykształcenie</h1>
        <p>10.2017 – obecnie : Uniwersytet im. Adama Mickiewicza w Poznaniu - informatyka - inżynierskie</p>
        <p>10.2016 – 03.2017: Politechnika Gdańska - Inżynieria Biomedyczna - inżynierskie</p>
        <p>09.2013 – 06.2016: Liceum Ogólnokształcące im. Komisji Edukacji Narodowej w Przasnyszu  - średnie</p>
    </section>

    <section id="languages">
        <h1>Znajomość języków</h1>
        <p>angielski: poziom średnio-zaawansowany</p>
        <p>polski: poziom ojczysty</p>
    </section>

    <section id="abilities">
        <h1>Umiejętności</h1>
        <ul>
            <li>PHP</li>
            <li>OOP</li>
            <li>CSS</li>
            <li>HTML</li>
            <li>Bootstrap</li>
            <li>Symfony</li>
            <li>Scrum</li>
            <li>PhpUnit</li>
            <li>Docker</li>
            <li>Rest Api</li>
            <li>MySQL</li>
            <li>PostgreSQL</li>
            <li>GIT</li>
        </ul>
    </section>

    <section id="training">
        <h1>Szkolenia</h1>
        <p>Eduweb</p>
    </section>
</article>
</body>

</html>