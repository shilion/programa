<?php

namespace Zadanie2345\Service\Menu;

use Zadanie2345\Repository\Menu\MenuRepository;

class MenuService
{
    private $menuRepository;

    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function getMenu(): array
    {
        return $this->buildHierarchy($this->menuRepository->getCategories());
    }

    private function buildHierarchy(
        array $categories = null,
        ?string $parentId = null
    ): array {
        $hierarchy = [];

        foreach ($categories as $category) {
            if ($category['parent_id'] === $parentId) {
                $child = $this->buildHierarchy($categories, $category['id']);
                $child ? $category['child'] = $child : null;
                $hierarchy[] = $category;
            }
        }

        return $hierarchy;
    }

    public function drawHierarchyAsHtml(
        array $menu
    ): string {
        $html = "";
        foreach ($menu as $category) {
            $html .= "<li>".$category["category_name"];
            $html .= "<ul>";
            !empty($category["child"]) ? $html .= $this->drawHierarchyAsHtml($category["child"]) : null;
            $html .= "</ul>";
            $html .= "</li>";
        }

        return $html;
    }
}
