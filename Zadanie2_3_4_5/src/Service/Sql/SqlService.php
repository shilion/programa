<?php

namespace Zadanie2345\Service\Sql;

use Zadanie2345\Traits\Sql\SqlConnectionTrait;

class SqlService
{
    use SqlConnectionTrait;

    public function createMenuTable(): void
    {
        $conn = $this->getConnection();

        $this->executeQuery($conn, $this->setCharset());
        $this->executeQuery($conn, $this->prepareMenuTableSql());
        $this->executeQuery($conn, $this->prepareMenuDataSql());
        $this->closeConnection($conn);
    }

    private function prepareMenuTableSql(): string
    {
        return "CREATE TABLE menu (id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
            "category_name VARCHAR(255) NOT NULL, parent_id INT(11) DEFAULT NULL, INDEX parent_id(parent_id), ".
            "CONSTRAINT FK_Parent FOREIGN KEY (parent_id) REFERENCES menu(id))";
    }

    private function prepareMenuDataSql(): string
    {
        return "INSERT INTO menu(category_name, parent_id) VALUES ('Kategoria główna', NULL), ".
            "('Podkategoria 1', 1),('Podkategoria 2', 1), ".
            "('Podkategoria 1.1', 2),('Podkategoria 1.2', 2),('Podkategoria 2.1', 3),('Podkategoria 2.2', 3)";
    }
}
