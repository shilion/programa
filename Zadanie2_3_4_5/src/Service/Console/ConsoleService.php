<?php

namespace Zadanie2345\Service\Console;

use Zadanie2345\Service\Sql\SqlService;

class ConsoleService
{
    private $sqlService;

    public function __construct(
        SqlService $sqlService
    ) {
        $this->sqlService = $sqlService;
    }

    public function exec(): void
    {
        $this->sqlService->createMenuTable();
    }
}
