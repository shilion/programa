<?php

namespace Zadanie2345\Traits\Sql;

trait SqlConnectionTrait
{
    use SqlConfigTrait;

    private function getConnection(): \mysqli
    {
        $conn = new \mysqli($this->serverName, $this->username, $this->password, $this->dbName);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }

    private function closeConnection(
        \mysqli $conn
    ) {
        $conn->close();
    }

    private function setCharset(): string
    {
        return "SET CHARSET utf8";
    }

    private function executeQuery(
        \mysqli $conn,
        string $sql,
        $output = true
    ) {
        if ($conn->query($sql) === true) {
            $output ? print "Query executed successfully" . PHP_EOL : null;
        } else {
            $output ? print "Error: " . $conn->error . PHP_EOL : null;
        }
    }
}
