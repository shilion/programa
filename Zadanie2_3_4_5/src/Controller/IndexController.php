<?php

namespace Zadanie2345\Controller;

use Zadanie2345\Service\Menu\MenuService;

class IndexController extends BaseController
{
    private $menuService;

    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
        $this->index();
    }

    public function index()
    {
        echo $this->render(
            __DIR__."/../../view/index.php",
            ["menu" => $this->menuService->drawHierarchyAsHtml($this->menuService->getMenu())]
        );
    }
}
