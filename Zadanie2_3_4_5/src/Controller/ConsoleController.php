<?php

namespace Zadanie2345\Controller;

use Zadanie2345\Service\Console\ConsoleService;
use Zadanie2345\Validator\CLIValidator;

class ConsoleController extends BaseController
{
    public function __construct(
        ConsoleService $console,
        CLIValidator $CLIValidator
    ) {
        $this->init($console, $CLIValidator);
    }

    public function init(
        ConsoleService $console,
        CLIValidator $CLIValidator
    ) {
        try {
            $CLIValidator->validate();
            $console->exec();
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }
}
