<?php

namespace Zadanie2345\Controller;

abstract class BaseController
{
    public function render($templateFile, array $vars = array())
    {
        ob_start();
        extract($vars);
        require($templateFile);

        return ob_get_clean();
    }
}
