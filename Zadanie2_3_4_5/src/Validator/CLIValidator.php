<?php

namespace Zadanie2345\Validator;

use Exception;

class CLIValidator
{
    private const MESSAGE = 'Only command line allowed!';

    /**
     * @throws Exception
     */
    public function validate(): void
    {
        $this->checkIfCli() ? null : $this->throwException(self::MESSAGE);
    }

    private function checkIfCli(): bool
    {
        return ((PHP_SAPI === 'cli'));
    }

    /**
     * @param string $message
     * @throws Exception
     */
    public function throwException(
        string $message
    ): void {
        throw new Exception($message);
    }
}
