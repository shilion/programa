<?php

namespace Zadanie2345\Repository\Menu;

use Zadanie2345\Traits\Sql\SqlConnectionTrait;

class MenuRepository
{
    use SqlConnectionTrait;

    public function getCategories(): array
    {
        $conn = $this->getConnection();

        $this->executeQuery($conn, $this->setCharset(), false);

        $mainCategory = $this->getMainCategory($conn);
        $categories = $this->findCategoriesByParent($conn, $mainCategory["id"]);

        $this->closeConnection($conn);

        array_unshift($categories, $mainCategory);

        return $categories;
    }

    public function getMainCategory(
        ?\mysqli $conn = null
    ): array {
        if ($conn === null) {
            $conn = $this->getConnection();
            $closeConnection = true;
        }

        $result = $conn->query('SELECT id, category_name, parent_id FROM menu WHERE parent_id IS NULL LIMIT 1');

        !empty($closeConnection) && $closeConnection === true ? $this->closeConnection($conn) : null;

        return $result->fetch_assoc();
    }

    private function findCategoriesByParent(
        \mysqli $conn,
        int $parentId
    ): array {
        $sql = "SELECT  id, category_name, parent_id FROM (SELECT * FROM menu ORDER BY parent_id, id) menu_sorted, (SELECT @pv := ".
            $parentId .
            ") initialisation WHERE find_in_set(parent_id, @pv) AND LENGTH(@pv := concat(@pv, ',', id))";

        $query = $conn->query($sql);

        return $query->fetch_all(MYSQLI_ASSOC);
    }
}
