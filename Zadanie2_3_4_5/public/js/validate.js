function validate()
{
    let inputs, returnValue = true, checked = false, radioName = "";
    let emptyFieldErr = "Field can not be empty", invalidAgeErr = "Age must be between 18 and 99!", invalidNumberErr = "Field must be a number!";

    inputs = document.getElementsByTagName("input");

    for (let input of inputs) {
        if (input.type === "radio" && input.checked) {
            checked = true
        } else if (input.type === "radio" && !input.checked) {
            radioName = input.name;
        }
        if (input.type === "checkbox" && !input.checked) {
            document.getElementById(input.name + "-error").innerHTML = emptyFieldErr;
            returnValue = false;
        }
        if (input.value === null || input.value === "") {
            document.getElementById(input.id + "-error").innerHTML = emptyFieldErr;
            returnValue = false;
        }
        if (input.id === "age" && input.value !== "" && isNaN(input.value)) {
            document.getElementById(input.id + "-error").innerHTML = invalidNumberErr;
            returnValue = false;
        }
        if (input.id === "age" && input.value !== "" && (input.value < 18 || input.value > 99)) {
            document.getElementById(input.id + "-error").innerHTML = invalidAgeErr;
            returnValue = false;
        }
    }

    if (checked !== true) {
        document.getElementById(radioName + "-error").innerHTML = emptyFieldErr;
        returnValue = false;
    }

    return returnValue;
}