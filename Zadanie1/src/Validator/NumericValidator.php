<?php

namespace Zadanie1\Validator;

use InvalidArgumentException;

class NumericValidator
{
    private const INVALID_ARGUMENT_MESSAGE = 'Invalid argument!';

    public function validate(
        string $value
    ): void {
        if ($value !== "") {
            $this->checkIfStringIsNumeric($value) ? null : $this->throwException(self::INVALID_ARGUMENT_MESSAGE);
        }
    }

    private function checkIfStringIsNumeric(
        string $value
    ): bool {
        return is_numeric($value) && $value > 0 && ($value * 10) % 10 === 0;
    }

    public function throwException(
        string $message
    ): void {
        throw new InvalidArgumentException($message);
    }
}
