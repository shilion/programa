<?php

namespace Zadanie1\Controller;

use Zadanie1\Service\Console\ConsoleService;
use Zadanie1\Validator\CLIValidator;

class IndexController
{
    public function __construct(
        ConsoleService $console,
        CLIValidator $CLIValidator
    ) {
        $this->init($console, $CLIValidator);
    }

    public function init(
        ConsoleService $console,
        CLIValidator $CLIValidator
    ) {
        try {
            $CLIValidator->validate();
            $console->exec();
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }
}
