<?php

namespace Zadanie1\Service\Console;

class OutputService
{
    public function write(
        string $message
    ): void {
        print $message;
    }
}
