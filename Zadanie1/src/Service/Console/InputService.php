<?php

namespace Zadanie1\Service\Console;

class InputService
{
    public function getInput(): string
    {
        return readline();
    }
}
