<?php

namespace Zadanie1\Service\Console;

use InvalidArgumentException;
use Zadanie1\Service\Calculation\CalculationService;
use Zadanie1\Validator\NumericValidator;

class ConsoleService
{
    private $input;

    private $output;

    private $validator;

    private $calculationService;

    public function __construct(
        InputService $input,
        OutputService $output,
        NumericValidator $validator,
        CalculationService $calculationService
    ) {
        $this->input = $input;
        $this->output = $output;
        $this->validator = $validator;
        $this->calculationService = $calculationService;
    }

    public function exec(): void
    {
        try {
            $allNumbers = [];
            while (true) {
                $this->output->write('Please enter number [leave blank to stop]: ');
                $this->validator->validate($number = $this->input->getInput());

                if ($number === null || $number === "") {
                    $this->output->write($this->calculationService->getResult($allNumbers));
                    break;
                } else {
                    $allNumbers[] = (int)$number;
                }
            }
        } catch (InvalidArgumentException $exception) {
            $this->output->write(PHP_EOL . $exception->getMessage());
        }
    }
}
