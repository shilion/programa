<?php

namespace Zadanie1\Service\Calculation;

class CalculationService
{
    private const EVEN_MULTIPLIER = 3;
    private const ODD_MULTIPLIER = 2;

    public const EVEN_EQUALS_ODD_MESSAGE = 'Even numbers multiplied by 3 ' .
    'are equal to odd numbers multiplied by 2';

    public const EVEN_HIGHER_MESSAGE = 'Even numbers multiplied by 3 ' .
    'are higher than odd numbers multiplied by 2';

    public const ODD_HIGHER_MESSAGE = 'Even numbers multiplied by 3 ' .
    'are lower than odd numbers multiplied by 2';

    public function getResult(
        array $numbers
    ): string {
        $evenSum = 0;
        $oddSum = 0;

        /** @var int $number */
        foreach ($numbers as $number) {
            $number % 2 === 0 ? $evenSum += $number : $oddSum += $number;
        }

        return $this->compareEvenAndOddSum($this->multiplyEven($evenSum), $this->multiplyOdd($oddSum));
    }

    private function multiplyEven(
        int $evenSum
    ): int {
        return $evenSum * self::EVEN_MULTIPLIER;
    }

    private function multiplyOdd(
        int $oddSum
    ): int {
        return $oddSum * self::ODD_MULTIPLIER;
    }

    private function compareEvenAndOddSum(
        int $evenSum,
        int $oddSum
    ): string {
        if ($evenSum === $oddSum) {
            return self::EVEN_EQUALS_ODD_MESSAGE;
        } elseif ($evenSum > $oddSum) {
            return self::EVEN_HIGHER_MESSAGE;
        } else {
            return self::ODD_HIGHER_MESSAGE;
        }
    }
}
