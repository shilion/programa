<?php

namespace Tests\Service\Calculation;

use PHPUnit\Framework\TestCase;
use Zadanie1\Service\Calculation\CalculationService;

class CalculationServiceTest extends TestCase
{
    private $calculationService;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculationService = new CalculationService();
    }

    public function testGetResultEquals()
    {
        self::assertEquals(CalculationService::EVEN_EQUALS_ODD_MESSAGE, $this->calculationService->getResult([2, 3]));
    }

    public function testGetResultEvenHigher()
    {
        self::assertEquals(CalculationService::EVEN_HIGHER_MESSAGE, $this->calculationService->getResult([2, 4, 6, 3]));
    }

    public function testGetResultOddHigher()
    {
        self::assertEquals(CalculationService::ODD_HIGHER_MESSAGE, $this->calculationService->getResult([3, 9, 13, 2]));
    }
}
