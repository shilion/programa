# Zadanie 1 
## Treść zadania
Napisz w języku PHP (7.*) klasę, które będzie przyjmowała podawane kolejno liczby naturalne, a na żądanie
użytkownika będzie pozwalała na uzyskanie informacji, czy większa była trzykrotność liczb parzystych, czy
dwukrotność nieparzystych.
Napisz test jednostkowy weryfikujący poprawność działania kodu.

## Wstęp
Postanowiłem rozbić dane zadanie na wiele różnych klas, zamiast zamieszczać całość kodu w jednej. Chciałem uzyskać bardziej optymalny i bardziej otwarty na rozszerzenia kod.

Aplikacja możliwa jest do uruchiomienia z poziomu terminala.

## Wykorzystane biblioteki
 - `php-di/php-di`
 - `phpunit/phpunit`
 
## Start aplikacji
 ```
 composer install
 php index.php 
 ```
 
 ## Uruchomienie testów
 
 ```
 ./vendor/bin/phpunit tests
```