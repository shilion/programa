<?php

$container = require './app/bootstrap.php';

try {
    $container->get(\Zadanie1\Controller\IndexController::class);
} catch (Exception $ex) {
    echo "Exception: " . $ex->getMessage();
}
